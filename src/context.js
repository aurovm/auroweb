
const debug = require("./debug.js");

function compute (parsed) {
  // TODO: Move to a file dedicated to contexts
  var contexts = {};
  var items = {};
  var unprocessed = [];

  function addItem (kind, index, data) {
    var key = kind + index;
    var item = { key, kind, index, data };
    items[key] = item;
    unprocessed.push(item);
  }

  for (var i = 0; i < parsed.modules.length; i++) {
    // Module 0 is reserved
    addItem("m", i+1, parsed.modules[i]);
  }

  for (var i = 0; i < parsed.types.length; i++) {
    addItem("t", i, parsed.types[i]);
  }

  for (var i = 0; i < parsed.functions.length; i++) {
    addItem("f", i, parsed.functions[i]);
  }

  var rootNode = {
    id: "_",
    key: null,
    parent: null,
    children: [],
  };

  var nodes = [];
  nodes.push(rootNode);

  // All context names that are excluded from the computation.
  // Regardless of the real context of these items, they will always evaluate as being from the root context.
  var skips = {};

  function isChild (child, parent) {
    while (child) {
      if (child == parent) {
        return true;
      }
      child = child.parent;
    }

    return false;
  }

  function min (a, b) {
    if (a == null || b == null) return null;

    if (isChild(a, b)) return a;
    if (isChild(b, a)) return b;

    return null;
  }

  function newChild (parent) {
    var child = { id: String(nodes.length), parent, children: [] };
    nodes.push(child);
    parent.children.push(child);
    return child;
  }

  function itemContext (key) {
    var item = items[key];
    var context = contexts[key];
    if (context) return context;
    else context = null;

    debug.log("ctx", key, item.data.code);
    debug.indent(true);

    var kind = item.kind + "." + item.data.type;
    switch (kind) {
      case "m.import": {
        context = rootNode;
        break;
      }
      case "m.define": {
        context = rootNode;
        for (var it of item.data.items) {
          let itKind = it.type[0];
          context = min(context, itemContext(itKind + it.index));
        }
        break;
      }
      case "m.use": {
        context = itemContext("m" + item.data.module);
        break;
      }
      case "m.build": {
        context = min(
          itemContext("m" + item.data.base),
          itemContext("m" + item.data.argument)
        );
        break;
      }
      case "m.functor": {
        // It's also possible to change the context map here,
        // to cache the computations of the inner nodes,
        // and then update the map in every call to itemContext,
        // instead of the outer loop.
        var baseKey = "m" + (item.index+1);
        debug.log("Functor", key, "skipping", baseKey);
        debug.indent(true);
        skips[baseKey] = true;
        context = itemContext("m" + (item.index + 2));
        debug.indent(false);
        debug.log("unskip", baseKey, "result", context);
        skips[baseKey] = false;
        break;
      }
      case "m.arg": {
        if (skips[key]) {
          context = rootNode;
          break;
        }

        var parent = itemContext("m" + (item.index - 1));
        if (parent) {
          context = newChild(parent);
          debug.log("Create context", key, context);
        }
        break;
      }
      case "t.import": {
        context = itemContext("m" + item.data.module);
        break;
      }
      case "f.bin":
      case "f.int": {
        context = rootNode;
        break;
      }
      case "f.call": {
        context = itemContext("f" + item.data.index);
        break;
      }
      case "f.import": {
        context = itemContext("m" + item.data.module);
        if (!context) break;

        for (var ti of item.data.ins) {
          context = min(context, itemContext("t" + ti));
        }
        for (var ti of item.data.outs) {
          context = min(context, itemContext("t" + ti));
        }
        break;
      }
      case "f.code": {
        context = rootNode;

        //debug.log(item.data.code);

        for (var inst of item.data.code) {
          if (inst.type == "call") {
            context = min(context, itemContext("f" + inst.index));
          }
        }

        for (var ti of item.data.ins) {
          context = min(context, itemContext("t" + ti));
        }
        for (var ti of item.data.outs) {
          context = min(context, itemContext("t" + ti));
        }
        break;
      }
      default: {
        throw new Error("unknown item kind: " + kind);
      }
    }
    debug.indent(false);
    debug.log("=>", context && context.id);

    return context;
  }

  while (unprocessed.length > 0) {
    var lastLength = unprocessed.length;

    for (var i = 0; i < unprocessed.length; i++) {
      var item = unprocessed[i];

      var context = itemContext(item.key);

      if (context) {
        // This can better serve for caching if the context map
        // is changed every time a functor context enters,
        // and this update is moved inside of itemContext
        contexts[item.key] = context;
        unprocessed.splice(i, 1);
        i--;
      }
    }

    if (unprocessed.length == lastLength) {
      console.error("unprocessed", unprocessed);
      console.log("contexts", contexts)
      throw new Error("compute context got stuck");
    }
  }

  function printnodes () {
    var nodeInfo = {};

    for (var node of nodes) {
      nodeInfo[node.id] = {
        items: [],
        children: node.children.map(n => n.id),
      };
    }

    for (var item of Object.values(items)) {
      var nodeId = contexts[item.key].id;
      nodeInfo[nodeId].items.push(item.key)
    }

    for (var nodeId in nodeInfo) {
      var info = nodeInfo[nodeId];

      console.log(
        `${nodeId} => ${info.children} {${info.items}}`
      );
    }
  }

  var result = {
    nodes: nodes.map(n => n.id),
    ancestry: {},
    items: {},
  };

  for (var node of nodes) {
    result.ancestry[node.id] = node.parent && node.parent.id;
  }

  for (var key in contexts) {
    result.items[key] = contexts[key].id;
  }

  //console.log(result); process.exit(0);

  return result;
}

function createStack (contextInfo, moduleName) {
  var mylog = () => {};
  if (true || moduleName == "utils\x1fprotocol") {
    mylog = debug.log;
  }

  var modId = "[" + moduleName + "]";

  const contextStack = {
    headCounter: 1,
    head: {
      id: "_G",
      ctx: '_',
      argument: null,
      cache: {},
      tail: null, 
    },
    states: [],
    replaceStack: [],
    find: function (ctx) {
      mylog("find", ctx)
      var head = contextStack.head;
      while (head) {
        if (head.ctx == ctx) {
          return head.argument;
        }
        head = head.tail;
      }
      return null;
    },
    push: function (ctx, argument) {
      mylog("push", ctx, argument.name)
      debug.stack(null, 1);
      debug.indent(true)
      contextStack.head = {
        id: this.headCounter++,
        ctx, argument,
        tail: contextStack.head,
        cache: {},
      };
      mylog("ID", contextStack.head.id)
    },
    pop: function () {
      debug.indent(false)
      mylog("pop")
      contextStack.head = contextStack.head.tail;
    },
    save: function () {
      var id = this.states.length;
      mylog("save", '#' + id, '=', this.head.ctx)
      this.states.push({
        head: this.head,
      });

      return id;
    },
    replace: function (id) {
      mylog("replace", id)
      debug.indent(true);
      this.replaceStack.push(this.head);
      this.head = this.states[id].head;

      var head = this.head;
      mylog("ID", head.id)
      while (head.tail) {
        mylog("-", head.ctx, head.argument.name)
        head = head.tail;
      }
    },
    restore: function () {
      debug.indent(false);
      mylog("restore")
      this.head = this.replaceStack.pop();
    },
    createCache: function (id) {
      var cache = {
        id,
        get: function (key) {
          key = this.id + key;
          var head = contextStack.head;
          while (head) {
            var cached = head.cache[key];
            if (cached) {
              mylog("hit ", key)
              return cached;
            }
            else head = head.tail;
          }
          mylog("miss ", key)
        },
        store: function (key, value) {
          key = this.id + key;
          var ctx = contextInfo.items[key];

          mylog("store", ctx, key);

          var head = contextStack.head;
          while (head) {
            if (head.ctx == ctx) {
              head.cache[key] = value;
              return value;
            }
            else head = head.tail;
          }

          console.log(moduleName)
          console.log({
            nodes: contextInfo.nodes,
            ancestry: contextInfo.ancestry
          });
          throw new Error(`context ${ctx} for key ${key} not found in context stack for ${moduleName}`);
        },
      };

      return cache;
    },
  };
  return contextStack;
}

Object.assign(exports, { compute, createStack });