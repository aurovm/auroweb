
var _indent = "";

exports.log = function () {
  var args = Array.from(arguments);
  if (_indent) args.unshift(_indent);
  //console.log.apply(console, args)
}

exports.indent = function (inc) {
  if (inc) {
    _indent += "│ "
  } else {
    _indent = _indent.slice(2)
  }
}

exports.stack = function (lines, skip) {
  if (skip == null) skip = 0;
  skip++;

  var end = undefined;

  if (lines) {
    end = lines + skip;
  } else {
    end = undefined;
  }

  var splat = new Error().stack.split("\n").slice(skip, end);
  for (var line of splat) exports.log(line);
}