TEST="term-canvas.au"

if [ "$1" == "build" ]; then
    echo build
  auro aulang modules-src/runtime.au modules/auroweb.runtime
  exit
fi

if [ "$1" == "install" ]; then\
  # Run same file
  bash ${BASH_SOURCE[0]} build

  echo install

  cd modules
  for a in $FILES; do
    auro --install $a
  done
  exit
fi

nugget aulang.v3 "tests/$TEST" out &&
node src/main.js --node -o out.js out &&
node out.js